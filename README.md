## Threadsafe Webdriver

This is a maven project that enables tests to be run in parallel using a configurable number of threads and threadsafe webdriver instances.

#### Browsers supported
Supports running tests on Firefox, Chrome, IE, Edge and Grid (remote).

#### Driver dependency management
Uses WebDriverManager to manage the driver dependencies. 

#### Running tests
- Run using all defaults as set in pom.xml: `mvn verify` or `mvn clean install`

Configurable parameters:
- Set the thread count (defaults to 1): `-Dthreads=4`
- Set the browser (defaults to firefox): `-Dbrowser=chrome`
- Set the profile to run (defaults to selenium): `-P<profile>`
- Set if you wish tests to run headlessly (firefox and chrome only, defaults to false): `-Dheadless=true`

Example, run the tests against the profile "selenium" across 4 Chrome threads:
 
`mvn clean install -Dthreads=4 -Dbrowser=chrome -Pselenium`

To run tests via Intellij, you need to set up a run configuration for Maven, and in the run field enter the maven command e.g `clean install -Dthreads=4 -Dbrowser=chrome`

#### Running tests on Grid
Requires working grid instance, e.g. SauceLabs

Configurable parameters:
- Set to run on remote (defaults to false): `Dremote=true`
- Set the gridURL (cannot be null): `-DgridURL=http://{username}:{accesskey}@ondemand.saucelabs.com:80/wd/hub`
- Set the platform (cannot be null): `-Dplatform=WIN10`
- Set the browserVersion (cannot be null): `-DbrowserVersion=72.0`

Example:

`mvn clean install -Dremote=true -DgridURL=http://{username}:{accesskey}@ondemand.saucelabs.com:80/wd/hub -Dplatform=windows10 -Dbrowser=firefox -DbrowserVersion=72.0`

`mvn clean install -Dremote=true -Dbrowser=firefox -DgridURL=http://{DockerVMURL}:4444/wd/hub -Dplatform=LINUX -DbrowserVersion=75.0`