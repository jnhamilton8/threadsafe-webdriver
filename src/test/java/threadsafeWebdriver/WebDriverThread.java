package threadsafeWebdriver;

import threadsafeWebdriver.config.DriverType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebDriverBuilder;

import java.net.MalformedURLException;
import java.net.URL;

import static threadsafeWebdriver.config.DriverType.*;
import static threadsafeWebdriver.config.LoadSystemProperties.*;

public class WebDriverThread {

    private WebDriver webdriver;
    private DriverType selectedDriverType;

    private final DriverType defaultDriverType = FIREFOX;

    private static final Logger LOG = LogManager.getLogger(WebDriverThread.class);

    public WebDriver getDriver() {

        if (webdriver == null) {
            selectedDriverType = determineEffectiveDriverType();
            MutableCapabilities capabilities = selectedDriverType.getCapabilities();
            checkForHeadless(selectedDriverType);

            instantiateWebDriver(capabilities);
        }

        return webdriver;
    }

    public void quitDriver() {
        if (webdriver != null) {
            webdriver.quit();
        }
    }

    private DriverType determineEffectiveDriverType() {
        DriverType driverType = defaultDriverType;

        try {
            driverType = Enum.valueOf(DriverType.class, getDesiredBrowser());

        } catch (IllegalArgumentException ignored) {
            LOG.error("Unknown driver specified, defaulting to '" + driverType + "'...");
        }

        return driverType;
    }

    private void instantiateWebDriver(MutableCapabilities capabilities) {
        LOG.info("Current Operating System: " + OPERATING_SYSTEM);
        LOG.info("Current Architecture: " + SYSTEM_ARCHITECTURE);
        LOG.info("Current Browser Selection: " + selectedDriverType);

        if (USE_REMOTE_WEBDRIVER) {

            URL seleniumGridURL;
            try {
                seleniumGridURL = new URL(SELENIUM_GRID_URL);
            } catch (MalformedURLException e) {
                throw new WebDriverException("Selenium Grid URL is not valid, please check!");
            }

            /*
            The RemoteWebDriverBuilder uses the Capabilities interface to set the capabilities
            setCapability will return IllegalArgumentException("Null values are not allowed") if values are null
            setCapability will return IllegalArgumentException("Capability is not valid"); if the capabilityName does not exist
            */
            RemoteWebDriverBuilder builder = RemoteWebDriver.builder()
                    .address(seleniumGridURL)
                    .addAlternative(capabilities)
                    .setCapability("browserVersion", DESIRED_BROWSER_VERSION)
                    .setCapability("platformName", getDesiredPlatform());

            webdriver = builder.build();

        } else {
            webdriver = selectedDriverType.getWebDriverObject(capabilities);
        }
    }

    private void checkForHeadless(DriverType selectedDriverType) {
        if ((selectedDriverType.equals(EDGE) || selectedDriverType.equals(IE))
                && isHeadless()) {
            throw new WebDriverException("Headless mode only supported on Chrome or Firefox");
        }
    }
}
