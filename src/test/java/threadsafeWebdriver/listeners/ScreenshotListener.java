package threadsafeWebdriver.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import threadsafeWebdriver.DriverFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ScreenshotListener extends TestListenerAdapter {

    private static final Logger LOG = LogManager.getLogger(ScreenshotListener.class);

    private boolean createFile(File screenshot) throws IOException {
        boolean fileCreated = false;

        if (screenshot.exists()) {
            fileCreated = true;
        } else {
            File parentDirectory = new File(screenshot.getParent());
            if (parentDirectory.exists() || parentDirectory.mkdirs()) {
                fileCreated = screenshot.createNewFile();
            }
        }
        return fileCreated;
    }

    private void writeScreenshotToFile(WebDriver driver, File screenshot) throws IOException {
        FileOutputStream screenshotStream = new FileOutputStream(screenshot);
        screenshotStream.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
        screenshotStream.close();
    }

    /**
     * Override TestNGs onTestFailure method to take a screenshot if a test fails
     * Catch any exceptions thrown and log, rather than halt the test in the screenshot fails
     * Augment the driver implementation if it cannot be cast into TakesScreenshot object
     *
     * @param failingTest describes the result of the test
     */
    @Override
    public void onTestFailure(ITestResult failingTest) {
        try {
            WebDriver driver = DriverFactory.getDriver();
            String screenshotDirectory = System.getProperty("user.dir") + "/screenshots/";
            String screenshotAbsolutePath = screenshotDirectory + File.separator + System.currentTimeMillis() + "_" +
                    failingTest.getName() + ".png";
            File screenshot = new File(screenshotAbsolutePath);
            if (createFile(screenshot)) {
                try {
                    writeScreenshotToFile(driver, screenshot);
                } catch (ClassCastException weNeedToAugmentOurDriverObject) {
                    writeScreenshotToFile(new Augmenter().augment(driver), screenshot);
                }
                LOG.info("Written screenshot to " + screenshotAbsolutePath);
            } else {
                LOG.error("Unable to create " + screenshotAbsolutePath);
            }
        } catch (Exception ex) {
            LOG.error("Unable to capture screenshot...");
            ex.printStackTrace();
        }
    }
}
