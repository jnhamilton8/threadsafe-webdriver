package threadsafeWebdriver.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class Helpers {
    /**
     * Helper method to determine if jQuery has any outstanding active AJAX requests
     *
     * @return true if jquery has finished running the AJAX call, otherwise false
     */
    public static ExpectedCondition<Boolean> jqueryAJAXCallsHaveCompleted() {
        return driver -> {
            if (driver != null) {
                return (Boolean) ((JavascriptExecutor) driver).executeScript("return (window.jQuery !=" +
                        "null) && (jQuery.active === 0;");
            }
            return false;
        };
    }

    /**
     * Helper function to find an element
     * Example: WebElement foo = AdditionalConditions.wait(driver).until(driver1 -> driver1.findElement(By.id("id")));
     *
     * @param driver instance of WebDriver
     * @return a instance of FluentWait
     */

    public static Wait<WebDriver> wait(WebDriver driver) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(15))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchElementException.class)
                .withMessage("Oh no! I timed out....the element was not found");
    }

    /**
     * Helper function to find an element
     *
     * @param driver instance of WebDriver
     * @param By the By selector to find the WebElement e.g. By.id("id")
     * @return the WebElement what matches that selector
     */
    BiFunction<WebDriver, By, WebElement> findElement = WebDriver::findElement;

    /**
     * Helper function to indicate if an element is found
     *
     * @param driver instance of WebDriver
     * @param By the By selector to find the WebElement e.g. By.id("id")
     * @return true if an element matches the selector, otherwise false
     */
    BiPredicate<WebDriver, By> isElementFound = (driver, selector) -> driver.findElements(selector).size() > 0;
}
