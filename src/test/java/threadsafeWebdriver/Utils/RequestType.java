package threadsafeWebdriver.Utils;

import org.apache.http.client.methods.*;

/**
 *  Enum to define the HTTP request types
 */
public enum RequestType {
    //all the below extend HttpRequestBase
    OPTIONS(new HttpOptions()),
    GET(new HttpGet()),
    POST(new HttpPost()),
    PUT(new HttpPut()),
    DELETE(new HttpDelete());

    private final HttpRequestBase requestMethod;

    RequestType(HttpRequestBase requestMethod) {
        this.requestMethod = requestMethod;
    }

    public HttpRequestBase getRequestMethod() {
        return this.requestMethod;
    }
}