package threadsafeWebdriver.Utils;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Set;

/**
 * Class to check if sending an HTTP GET request to a URL results in a valid response
 */
public class FileDownloader {
    private RequestType httpRequestMethod = RequestType.GET;
    private URI fileURI;
    List<NameValuePair> urlParameters;
    private WebDriver driver;

    public FileDownloader(WebDriver driverObject) {
        this.driver = driverObject;
    }

    /**
     * Sets the type of the HTTP request you wish to send
     */
    public void setHTTPRequestMethod(RequestType requestType) {
        httpRequestMethod = requestType;
    }

    /**
     * Adds URL params to request if required e.g for POST/PUT requests
     */
    public void setURLParameters(List<NameValuePair> urlParameters) {
        this.urlParameters = urlParameters;
    }

    public void setURI(URI linkToFile) {
        fileURI = linkToFile;
    }

    /**
     * Negotiates with the remote server associated with the file URI
     * Allows us to simulate the request coming from the browser by getting the webdriver user agent
     * and cookies and using those in the request
     *
     * @throws IOException, NullPointerException
     */
    private HttpResponse makeHTTPConnection() throws IOException, NullPointerException {
        if (fileURI == null) throw new NullPointerException("No file URI specified");

        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpRequestBase requestMethod = httpRequestMethod.getRequestMethod();
        requestMethod.setURI(fileURI);

        BasicHttpContext localContext = new BasicHttpContext();

        localContext.setAttribute(
                HttpClientContext.COOKIE_STORE,
                getWebDriverCookies(driver.manage().getCookies())
        );

        requestMethod.setHeader("User-Agent", getWebDriverUserAgent());

        if (urlParameters != null && (
                httpRequestMethod.equals(RequestType.POST) || httpRequestMethod.equals(RequestType.PUT)
        )) {
            ((HttpEntityEnclosingRequestBase) requestMethod).setEntity(new UrlEncodedFormEntity(urlParameters));
        }
        return client.execute(requestMethod, localContext);
    }

    /**
     * Negotiates connection with remote server and gets the HTTP status code for a file
     *
     * @return httpStatusCode the http status code of the response
     * @throws IOException, NullPointerException
     */
    public int getLinkHTTPStatus() throws IOException, NullPointerException {
        HttpResponse downloadableFile = makeHTTPConnection();
        int httpStatusCode;
        try {
            httpStatusCode = downloadableFile.getStatusLine().getStatusCode();
        } finally {
            if (downloadableFile.getEntity() != null) {
                downloadableFile.getEntity().getContent().close();
            }
        }
        return httpStatusCode;
    }

    /**
     * Copy the current user agent
     * @return the user agent being used
     */
    private String getWebDriverUserAgent() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return js.executeScript("return navigator.userAgent").toString();
    }

    /**
     * Copy the cookies to allow a file HTTP request to simulate coming from the browser
     *
     * @param seleniumCookieSet set of cookies used by Selenium
     * @return a set containing a copy of the cookies in use by selenium
     */
    private BasicCookieStore getWebDriverCookies(Set<Cookie> seleniumCookieSet) {
        BasicCookieStore copyOfWebDriverCookieStore = new BasicCookieStore();

        for (Cookie seleniumCookie : seleniumCookieSet) {
            BasicClientCookie duplicateCookie = new BasicClientCookie(
                    seleniumCookie.getName(),
                    seleniumCookie.getValue());

            duplicateCookie.setDomain(seleniumCookie.getDomain());
            duplicateCookie.setSecure(seleniumCookie.isSecure());
            duplicateCookie.setExpiryDate(seleniumCookie.getExpiry());
            duplicateCookie.setPath(seleniumCookie.getPath());

            copyOfWebDriverCookieStore.addCookie(duplicateCookie);
        }
        return copyOfWebDriverCookieStore;
    }

    public File downloadFile() throws IOException {
        File downloadedFile = File.createTempFile("download", ".pdf");
        HttpResponse fileToDownload = makeHTTPConnection();

        try {
            FileUtils.copyInputStreamToFile(fileToDownload.getEntity().getContent(), downloadedFile);
        } finally {
            fileToDownload.getEntity().getContent().close();
        }
        return downloadedFile;
    }
}
