package threadsafeWebdriver.Utils;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

/**
 * Class that calculates an offset between two elements
 * For when you do not wish to use the default trigger position of the element
 * Assumes we wish to click the parent element
 * NOTE: when using offsets do not use clickAndHold as it will just click in the centre!
 */
public class CalculateOffsetPosition {

    /**
     * The default click position is either top left of centre
     * Depending on the method used to click e.g. movetoElement is based on top left
     * whereas moveByOffset is based on the centre
     */
    public enum CursorPosition {
        TOP_LEFT,
        CENTER
    }

    final WebElement parentElement;
    final WebElement childElement;
    final CursorPosition cursorPosition;
    private int xOffset;
    private int yOffset;

    public CalculateOffsetPosition(
            WebElement parentElement,
            WebElement childElement,
            CursorPosition cursorPosition) {
        this.parentElement = parentElement;
        this.childElement = childElement;
        this.cursorPosition = cursorPosition;
        calculateOffset();
    }

    public int getXOffset() {
        return xOffset;
    }

    public int getYOffset() {
        return yOffset;
    }

    public void calculateOffset() throws ElementNotVisibleException {
        int elementOneHeight = parentElement.getSize().getHeight();
        int elementOneWidth = parentElement.getSize().getWidth();
        int elementTwoHeight = childElement.getSize().getHeight();
        int elementTwoWidth = childElement.getSize().getWidth();

        if (elementTwoHeight >= elementOneHeight && elementTwoWidth >= elementOneWidth) {
            throw new ElementNotVisibleException("The child element is totally covering the parent element");
        }

        if (cursorPosition.equals(CursorPosition.TOP_LEFT)) {
            xOffset = 1;
            yOffset = 1;
        }

         //work out how far to the right or up we need to go to click the parent element
         //if the cursor clicks in the center by default
        if (cursorPosition.equals(CursorPosition.CENTER)) {
            if (elementTwoWidth < elementOneWidth) {
                xOffset = (elementTwoWidth / 2) + 1;
            }
            if (elementTwoHeight < elementOneHeight) {
                yOffset = (elementTwoHeight / 2) + 1;
            }
        }
    }
}
