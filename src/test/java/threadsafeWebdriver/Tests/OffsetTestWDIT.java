package threadsafeWebdriver.Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import threadsafeWebdriver.DriverFactory;
import threadsafeWebdriver.Utils.CalculateOffsetPosition;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Example test for demonstrating using Offsets - it will not run, it is for demo purposes only
 */
public class OffsetTestWDIT extends DriverFactory {
    WebDriver driver;
    Actions advancedActions;

    @BeforeMethod
    public void setup() {
        driver = getDriver();
        driver.get("http://dummyURL");
        advancedActions = new Actions(driver);
    }

    @Test
    void canUseOffsetsToClickAParentElement() {
        final By destroyableBoxes = By.cssSelector("ul > li > div");
        WebElement obliteratorBox = driver.findElement(By.id("obliterate"));
        WebElement firstBox = driver.findElement(By.id("one"));
        WebElement firstBoxText = driver.findElement(By.cssSelector("#one > span"));

        assertThat(driver.findElements(destroyableBoxes).size(), is(equalTo(5)));

        CalculateOffsetPosition op = new CalculateOffsetPosition(firstBox, firstBoxText, CalculateOffsetPosition.CursorPosition.CENTER);

        advancedActions.moveToElement(firstBox)
                .moveByOffset(op.getXOffset(), op.getYOffset())
                .clickAndHold()
                .moveToElement(obliteratorBox)
                .release()
                .perform();

        assertThat(driver.findElements(destroyableBoxes).size(), is(equalTo(4)));
    }
}
