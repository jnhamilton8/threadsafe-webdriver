package threadsafeWebdriver.Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import threadsafeWebdriver.DriverFactory;
import threadsafeWebdriver.Utils.FileDownloader;
import threadsafeWebdriver.Utils.RequestType;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static threadsafeWebdriver.Utils.CheckFileHash.generateHashForFileOfType;
import static threadsafeWebdriver.Utils.HashType.MD5;

/**
 * Example test for demonstrating checking file URIs are valid - it will not run, it is for demo purposes only
 */
public class FileDownloadTestWDIT extends DriverFactory {
    WebDriver driver;

    @BeforeMethod
    public void setup() {
        driver = getDriver();
        driver.get("http://dummyURLWithFileDownloadButton");
    }

    @Test
    void validFileDownloadURIReturnsStatus200() throws IOException, URISyntaxException {
        FileDownloader downloadHandler = new FileDownloader(driver);
        WebElement fileThatShouldExist = driver.findElement(By.id("fileToDownload"));
        URI fileAsURI = new URI(fileThatShouldExist.getAttribute("href"));

        downloadHandler.setURI(fileAsURI);
        downloadHandler.setHTTPRequestMethod(RequestType.GET);

        assertThat(downloadHandler.getLinkHTTPStatus(), is(equalTo(200)));
    }

    @Test
    void canDownloadFile() throws IOException, URISyntaxException {
        FileDownloader downloadHandler = new FileDownloader(driver);
        WebElement fileThatShouldExist = driver.findElement(By.id("fileToDownload"));
        URI fileAsURI = new URI(fileThatShouldExist.getAttribute("href"));
        downloadHandler.setURI(fileAsURI);
        downloadHandler.setHTTPRequestMethod(RequestType.GET);

        File downloadedFile = downloadHandler.downloadFile();

        assertThat(downloadedFile.exists(), is(equalTo(true)));
        assertThat(downloadHandler.getLinkHTTPStatus(), is(equalTo(200)));
    }

    @Test
    void downloadedFileHasSameMD5HashValueAsKnownValidFile() throws Exception {
        FileDownloader downloadHandler = new FileDownloader(driver);
        WebElement fileThatShouldExist = driver.findElement(By.id("fileToDownload"));
        URI fileAsURI = new URI(fileThatShouldExist.getAttribute("href"));
        downloadHandler.setURI(fileAsURI);
        downloadHandler.setHTTPRequestMethod(RequestType.GET);

        File downloadedFile = downloadHandler.downloadFile();

        assertThat(downloadedFile.exists(), is(equalTo(true)));
        assertThat(downloadHandler.getLinkHTTPStatus(), is(equalTo(200)));
        assertThat(generateHashForFileOfType(downloadedFile, MD5), is(equalTo("098f6bcd4621d373cade4e832627b4f6")));
    }
}