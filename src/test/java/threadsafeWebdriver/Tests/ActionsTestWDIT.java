package threadsafeWebdriver.Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import threadsafeWebdriver.DriverFactory;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Example test for demonstrating Actions - it will not run, it is for demo purposes only
 */

public class ActionsTestWDIT extends DriverFactory {
    WebDriver driver;
    Actions advancedActions;

    @BeforeMethod
    public void setup() {
        driver = getDriver();
        driver.get("http://dummyURL");
        advancedActions = new Actions(driver);
    }

    @Test
    void canHoverOverMenusAndSelect() {
        WebDriverWait wait = new WebDriverWait(driver, 5, 100);

        WebElement servicesMenuOption = driver.findElement(By.id("Services"));
        WebElement webDevelopmentSubMenuOption = driver.findElement(By.cssSelector("#services > ul > li:nth-child(2)"));

        //move mouse and make it hover over the Services menu option
        advancedActions.moveToElement(servicesMenuOption).perform();

        //Wait for the submenu to appear
        wait.until(ExpectedConditions.visibilityOf(webDevelopmentSubMenuOption));

        //Move mouse down to submenu and click on it
        advancedActions.moveToElement(webDevelopmentSubMenuOption)
                .click()
                .perform();
    }

    @Test
    void canDragAndDrop() {
        final By destroyableBoxes = By.cssSelector("ul > li > div");
        WebElement obliteratorBox = driver.findElement(By.id("obliterate"));
        WebElement firstBox = driver.findElement(By.id("one"));
        WebElement secondBox = driver.findElement(By.id("two"));

        //firstly check that there are 5 boxes which we can "destroy"
        assertThat(driver.findElements(destroyableBoxes).size(), is(equalTo(5)));

        //click and hold the first box and move it to the obliteratorBox to destroy it, using the clickAndHold function
        advancedActions.clickAndHold(firstBox)
                .moveToElement(obliteratorBox)
                .release()
                .perform();

        //make sure our count has decreased i.e. the box was destroyed
        assertThat(driver.findElements(destroyableBoxes).size(), is(equalTo(4)));

        //drag and drop second box to the obliteratorBox to destroy it, using the dragAndDrop function
        advancedActions.dragAndDrop(secondBox, obliteratorBox).perform();
        assertThat(driver.findElements(destroyableBoxes).size(), is(equalTo(4)));
    }
}
