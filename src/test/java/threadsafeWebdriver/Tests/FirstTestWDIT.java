package threadsafeWebdriver.Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import threadsafeWebdriver.DriverFactory;

import java.time.Duration;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class FirstTestWDIT extends DriverFactory {

    private void googleExampleThatSearchesFor(final String searchString) {
        WebDriver driver = getDriver();
        driver.get("http://www.google.com");

        // clear the google search agree to terms popup
        driver.findElement(By.xpath("//div[contains(text(),'I agree')]")).click();

        var searchField = driver.findElement(By.name("q"));
        searchField.clear();
        searchField.sendKeys(searchString);
        searchField.submit();

        (new WebDriverWait(driver, Duration.ofSeconds(2))).until((ExpectedCondition<Boolean>) driverObject ->
                driverObject.getTitle().toLowerCase().startsWith(searchString.toLowerCase()));

        String title = driver.getTitle();
        System.out.println("Page title is: " + title);
        assertThat(title, containsString(searchString));
    }

    @Test
    public void googleCheeseExample() {
        googleExampleThatSearchesFor("Cheese!");
        System.out.println("Thread id = " + Thread.currentThread().getId());
    }

    @Test
    public void googleMilkExample() {
        googleExampleThatSearchesFor("Milk!");
        System.out.println("Thread id = " + Thread.currentThread().getId());
    }

    @Test
    public void googleCakeExample() {
        googleExampleThatSearchesFor("Cake!");
        System.out.println("Thread id = " + Thread.currentThread().getId());
    }

    @Test
    public void googleBreadExample() {
        googleExampleThatSearchesFor("Bread!");
        System.out.println("Thread id = " + Thread.currentThread().getId());
    }

    @Test
    public void googleVegExample() {
        googleExampleThatSearchesFor("Carrot!");
        System.out.println("Thread id = " + Thread.currentThread().getId());
    }

    @Test
    public void googleMeatExample() {
        googleExampleThatSearchesFor("Chicken!");
        System.out.println("Thread id = " + Thread.currentThread().getId());
    }
}