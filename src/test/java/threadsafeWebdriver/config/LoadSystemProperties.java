package threadsafeWebdriver.config;

public class LoadSystemProperties {

    public static final String OPERATING_SYSTEM = System.getProperty("os.name");
    public static final String SYSTEM_ARCHITECTURE = System.getProperty("os.arch");
    public static final String SELENIUM_GRID_URL = System.getProperty("gridURL");
    public static final String DESIRED_BROWSER_VERSION = System.getProperty("browserVersion");
    public static final boolean USE_REMOTE_WEBDRIVER = Boolean.getBoolean("remote");

    public static String getDesiredPlatform() {
        String desiredPlatform = System.getProperty("platform");

        if (desiredPlatform != null && !desiredPlatform.isEmpty()) {
            return desiredPlatform.toUpperCase();
        }

        return desiredPlatform;
    }

    public static String getDesiredBrowser() {
        String desiredBrowser = System.getProperty("browser");

        if (desiredBrowser != null && !desiredBrowser.isEmpty()) {
            return desiredBrowser.toUpperCase();
        }

        return desiredBrowser;
    }
}
