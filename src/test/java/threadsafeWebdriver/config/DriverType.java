package threadsafeWebdriver.config;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.Collections;
import java.util.HashMap;

public enum DriverType implements DriverSetup {

    FIREFOX {
        public WebDriver getWebDriverObject(MutableCapabilities options) {

            WebDriverManager.firefoxdriver().setup();

            return new FirefoxDriver((FirefoxOptions) options);
        }

        public FirefoxOptions getCapabilities() {
            FirefoxOptions options = new FirefoxOptions();

            if (isHeadless()) {
                options.setHeadless(true);
            }

            return options;
        }
    },
    CHROME {
        public WebDriver getWebDriverObject(MutableCapabilities options) {

            WebDriverManager.chromedriver().setup();

            return new ChromeDriver((ChromeOptions) options);
        }

        public ChromeOptions getCapabilities() {
            ChromeOptions options = new ChromeOptions();
            options.setCapability("chrome.switches", Collections.singletonList("--no-default-browser-check"));

            HashMap<String, String> chromePreferences = new HashMap<>();
            chromePreferences.put("profile.password_manager_enabled", "false");
            options.setCapability("chrome.prefs", chromePreferences);

            if (isHeadless()) {
                options.setHeadless(true);
            }

            return options;
        }
    },
    IE {
        public WebDriver getWebDriverObject(MutableCapabilities options) {

            WebDriverManager.iedriver().arch32().setup(); //64 bit version is way too slow and causes timeouts

            return new InternetExplorerDriver((InternetExplorerOptions) options);
        }

        public InternetExplorerOptions getCapabilities() {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.requireWindowFocus();
            options.enablePersistentHovering();

            return options;
        }
    },
    EDGE {
        public WebDriver getWebDriverObject(MutableCapabilities options) {

            WebDriverManager.edgedriver().setup();

            return new EdgeDriver((EdgeOptions) options);
        }

        public EdgeOptions getCapabilities() {

            return new EdgeOptions();
        }
    };

    public static boolean isHeadless() {
        return Boolean.parseBoolean(System.getProperty("headless"));
    }
}
