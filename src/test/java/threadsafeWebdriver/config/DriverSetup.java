package threadsafeWebdriver.config;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;

public interface DriverSetup {

     WebDriver getWebDriverObject(MutableCapabilities capabilities);

    <T extends MutableCapabilities> MutableCapabilities getCapabilities();
}