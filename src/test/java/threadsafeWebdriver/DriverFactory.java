package threadsafeWebdriver;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import threadsafeWebdriver.listeners.ScreenshotListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Listeners(ScreenshotListener.class) //all tests extend DriverFactory, so add @Listeners annotation here
public class DriverFactory {

    private static final List<WebDriverThread> webDriverThreadPool = Collections.synchronizedList(new ArrayList<>());
    private static ThreadLocal<WebDriverThread> driverThread;

    //as this class is extended by the test classes, the @BeforeSuite will run once - before each test run
    @BeforeSuite
    public static void instantiateDriverObject() {
        driverThread = ThreadLocal.withInitial(() -> {
            final WebDriverThread webDriverThread = new WebDriverThread();
            webDriverThreadPool.add(webDriverThread);
            return webDriverThread;
        });
    }

    /* The get() call returns driver in the current thread
      If the variable has no value for the current thread, it is first initialized to the value returned
      by an invocation of the initialValue method.
      It then calls getDriver to either instantiate the driver or return one if already present */
    public static WebDriver getDriver() {
        return driverThread.get().getDriver();
    }

    @BeforeMethod
    public static void clearCookies() {
        getDriver().manage().deleteAllCookies();
    }

    @AfterSuite
    public static void closeDriverObjects() {
        for (WebDriverThread webDriverThread : webDriverThreadPool) {
            webDriverThread.quitDriver();
        }
    }
}